package com.example.fleenor.mycalculator;

        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.app.Activity;
        import android.os.Bundle;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.webkit.WebView;
        import android.widget.Button;
        import android.widget.TextView;
        import java.util.LinkedList;
        import java.util.Queue;

public class CalculatorActivity extends Activity {

    private WebView mWebView;
    private TextView mTextView;
    private StringBuilder mMathString;
    private ButtonClickListener mClickListener;
    Queue<CharSequence> myqueue = new LinkedList<>();

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        // Create the math string
        mMathString = new StringBuilder();

        // Enable javascript for the view
        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);

        // Set the listener for all the buttons
        mClickListener = new ButtonClickListener();

        int idList[] = {
                            R.id.button0, R.id.button1, R.id.button2,
                            R.id.button3, R.id.button4, R.id.button5, R.id.button6,
                            R.id.button7, R.id.button8, R.id.button9, R.id.buttonLeftParen,
                            R.id.buttonRightParen, R.id.buttonPlus, R.id.buttonPlus,
                            R.id.buttonMinus, R.id.buttonDivide, R.id.buttonTimes,
                            R.id.buttonDecimal, R.id.buttonBackspace, R.id.buttonClear
                        };

        for (int id : idList) {
            View v = findViewById(id);
            v.setOnClickListener(mClickListener);
        }

    }

    private void updateWebView() {

        StringBuilder builder = new StringBuilder();

        builder.append("<html><body>");
        builder.append("<script type=\"text/javascript\">document.write('");
        builder.append(mMathString.toString());
        builder.append("');");
        builder.append("document.write('<br />=' + eval(\"");
        builder.append(mMathString.toString());
        builder.append("\"));</script>");
        builder.append("</body></html>");

        mWebView.loadData(builder.toString(), "text/html", "UTF-8");
    }

    private void updateTextView()  {
        mTextView.setText((myqueue.peek().toString()));
    }

    private class ButtonClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buttonBackspace:
                    if (mMathString.length() > 0)
                        mMathString.deleteCharAt(mMathString.length() - 1);

                    if (myqueue.size() > 0)
                            myqueue.remove();

                    break;
                case R.id.buttonClear:
                    if (mMathString.length() > 0)
                        mMathString.delete(0, mMathString.length());

                    if (myqueue.size() > 0)
                        for(int i = 0; i < myqueue.size();i++)
                        {
                            myqueue.remove();
                        }
                    break;

                case R.id.buttonEquals:
                    updateWebView();
                    updateTextView();
                    break;

                default:
                    myqueue.add(((Button) v).getText());
                    mMathString.append(((Button) v).getText());
                    break;

            }

            updateTextView();
            updateWebView();
        }

    }
}